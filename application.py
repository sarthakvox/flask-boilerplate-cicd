from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/ping")
def ping():
    return jsonify(status="pong"), 200

if __name__ == "__main__":
    app.run()