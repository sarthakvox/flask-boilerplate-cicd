FROM python:3.6-slim-buster

WORKDIR /usr/src/app

# Add requirements.txt before rest of repo for caching
COPY ./requirements.txt /usr/src/app/requirements.txt

RUN pip install -r requirements.txt

# This prevents loading backend before database service
# COPY wait-for-it.sh /usr/src/app/wait-for-it.sh
# RUN chmod +x wait-for-it.sh
# RUN chmod +x /usr/src/app/wait-for-it.sh

# RUn alembic db migrations
# COPY entrypoint.sh /usr/src/app/entrypoint.sh
# RUN chmod +x entrypoint.sh
# RUN chmod +x /usr/src/app/entrypoint.sh

# exposed in docker-compose file
EXPOSE 5000 

# Only use `--wokers 1` and `--worker-class eventlet` (eventlent or gevent) if you're using socket.io
# you need to install eventlet or gevent package
CMD gunicorn -w 1 -b 0.0.0.0:5000 application:app